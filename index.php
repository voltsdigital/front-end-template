<!DOCTYPE HTML>

<html>

<head>

	<link rel='stylesheet' type='text/css' href='public/css/style.css?v=1.0'>

	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<title>Your Website</title>

</head>

<body data-baseurl='.'>

	<div class='all'>

		<h1>Title 1</h1>
		<h2>Title 2</h2>
		<h3>Title 3</h3>
		<h4>Title 4</h4>
		<h5>Title 5</h5>
		<h6>Title 6</h6>

		<ul>
			<li>Item 1</li>
			<li>Item 2</li>
			<li>Item 3</li>
			<li>Item 4</li>
			<li>Item 5</li>
			<li>Item 6</li>
		</ul>

		<p>Lorem <a href='#' title='ipsum'>ipsum</a> dolor sit amet, <i>consectetur</i> adipisicing elit. <b>Obcaecati</b> praesentium, <u>natus</u> amet aperiam necessitatibus dolor illo id architecto, iste nam voluptatem quis culpa dignissimos reiciendis excepturi cum officiis. Illo, nam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati praesentium, natus amet aperiam necessitatibus dolor illo id architecto, iste nam voluptatem quis culpa dignissimos reiciendis excepturi cum officiis. Illo, nam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati praesentium, natus amet aperiam necessitatibus dolor illo id architecto, iste nam voluptatem quis culpa dignissimos reiciendis excepturi cum officiis. Illo, nam.</p>

		<form name='form_name' id='form_name' class='form_name'>
			
			<div class='xxx'>
				<label>name</label><br>
				<input type='text' name='xxx' placeholder='xxx' required='required'>
			</div>
			
			<div class='yyy'>
				<label>email</label><br>
				<input type='email' name='yyy' placeholder='yyy' required='required'>
			</div>
			
			<div class='zzz'>
				<label>phone</label><br>
				<input type='tel' name='zzz' class='phone' placeholder='zzz' minlength='14' required='required'>
			</div>

			<button type='submit'>Enviar</button>

		</form>

		<div class="swiper-container normal_slider">
			
		    <div class="swiper-wrapper">
		        <div class="swiper-slide">Slide 1</div>
		        <div class="swiper-slide">Slide 2</div>
		        <div class="swiper-slide">Slide 3</div>
		    </div>

		    <div class='normal_slider-left'></div>
		    <div class='normal_slider-right'></div>

		</div>

		<div class='gallery'>
			<a href='http://via.placeholder.com/400x400'><img src='http://via.placeholder.com/400x400'></a>
			<a href='http://via.placeholder.com/500x500'><img src='http://via.placeholder.com/500x500'></a>
			<a href='http://via.placeholder.com/600x600'><img src='http://via.placeholder.com/600x600'></a>
			<a href='http://via.placeholder.com/700x700'><img src='http://via.placeholder.com/700x700'></a>
		</div>

	</div>

	<script src='public/js/main.js'></script>

</body>

</html>
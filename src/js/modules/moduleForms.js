//
// moduleForms
//

function moduleForms(){
	window.moduleForms.applyPhoneMask();

	$('.form_name').validate({
		submitHandler : function(){
			alert('ajax!');
			return false;
		}
	});

}

//_ACTIONS
window.moduleForms.applyPhoneMask = function applyPhoneMask(){

	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$('.phone').mask(SPMaskBehavior, spOptions);

}

//_END
moduleForms();
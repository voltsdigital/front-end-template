//
// moduleSliders
//

function moduleSliders(){

	var normalSlider = new Swiper('.normal_slider', {
	    speed : 800,
	    spaceBetween : 20,
	    navigation : {
	    	nextEl : '.normal_slider-right',
	    	prevEl : '.normal_slider-left',
	    }
	});
	
}

//_ACTIONS

//_END
moduleSliders();